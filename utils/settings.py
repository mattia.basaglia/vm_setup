import os
import json
import inspect
import importlib.util


class SettingsData(object):
    def __init__(self, filepath):
        self.filepath = filepath
        if os.path.exists(filepath):
            with open(filepath, "r") as file:
                self.data = json.load(file)
        else:
            self.data = {}

    def get(self, key, default=None):
        return self.data.setdefault(key, default)

    def __getitem__(self, key):
        return self.data[key]

    def __setitem__(self, key, value):
        return self.set(key, value)

    def set(self, key, value):
        self.data[key] = value

    def save(self):
        with open(self.filepath, "w") as file:
            return json.dump(self.data, file, indent=4)

    def to_json(self, *args, **kwargs):
        json.dump(self.data, *args, **kwargs)


class Settings(SettingsData):
    def __init__(self, data_dir):
        self.data_dir = data_dir
        self.vm_dir = os.path.join(data_dir, "vm")
        self.settings = SettingsData(os.path.join(data_dir, "settings.json"))
        self.data = SettingsData(os.path.join(data_dir, "data.json"))
        self.commands = {}

    def load_commands(self, base_path):
        from .base_command import BaseCommand
        for path in os.listdir(base_path):
            if path.endswith(".py"):
                module_name = "commands.command"
                module = self.load_module(os.path.join(base_path, path), module_name)
                for name, object in inspect.getmembers(module):
                    if inspect.isclass(object) and issubclass(object, BaseCommand) \
                        and object not in ignore_command.list:
                        instance = object()
                        self.commands[instance.name()] = instance

    def load_module(self, filepath, name):
        spec = importlib.util.spec_from_file_location("commands.command", filepath)
        module = importlib.util.module_from_spec(spec)
        if os.path.exists(filepath):
            spec.loader.exec_module(module)
        return module

    def get(self, key, default=None):
        return self.settings.get(key, default)

    def save(self):
        self.data.save()

    def vm_path(self, *path):
        return os.path.join(self.vm_dir, *path)


def ignore_command(func):
    """
    Class decorator to avoid automatic command registration
    """
    ignore_command.list.add(func)
    return func
ignore_command.list = set()
