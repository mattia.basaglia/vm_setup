import re
import inspect
from subprocess import call
from .settings import ignore_command


def _namify(string):
    string = re.sub('([^-A-Z])([A-Z])', r'\1-\2', string)
    string = re.sub('([^A-Za-z0-9]+)', r'-', string)
    return string.lower()


@ignore_command
class BaseCommand(object):
    """
    Base class for vm commands
    """
    def name(self):
        """
        Name of the command
        """
        return _namify(self.__class__.__name__)

    def help(self):
        """
        Help string for the command
        """
        return inspect.getdoc(self)

    def arg_parser(self, subparsers):
        """
        Creates a command line parser
        """
        return subparsers.add_parser(self.name(), help=self.help())

    def arg_setup(self, subparsers, settings):
        """
        Sets up the command line parser
        """
        parser = self.arg_parser(subparsers)
        self.arg_arguments(parser, settings)
        return parser

    def arg_arguments(self, parser, settings):
        """
        Override to add custom options for the command,
        should also ensure settings has all of the required data initialized
        """
        pass

    def run(self, command_line, settings):
        """
        Override to add command behaviour
        """
        raise NotImplementedError()

    def cmd(self, args, **kwargs):
        """
        Runs a command as the current user
        """
        print(" ".join(args))
        call(args, **kwargs)
