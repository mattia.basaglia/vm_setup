#!/usr/bin/env python3

import os
import sys
import argparse
from utils.settings import Settings


parser = argparse.ArgumentParser(
    description="VM manager",
    add_help=False,
)
global_opts = parser.add_argument_group("Common options")
global_opts.add_argument(
    "--data",
    help="Data path for settings and similar",
    default=os.path.join(os.path.dirname(__file__), "data")
)
global_opts.add_argument(
    "--command-paths",
    help="Path used to search for commands",
    default=[os.path.join(os.path.dirname(__file__), "commands")],
    nargs="*",
)
global_opts.add_argument(
    "--help", "-h",
    help="Show this message and exit, pass after a command to het help for that command",
    action="help"
)
pre_parsed, args = parser.parse_known_args()
settings = Settings(pre_parsed.data)
subparsers = parser.add_subparsers(title="Commands", dest="command")
commands = {}

for path in pre_parsed.command_paths:
    settings.load_commands(path)

for command in settings.commands.values():
    command.arg_setup(subparsers, settings)

command_line = parser.parse_args(args)

if command_line.command in settings.commands:
    settings.commands[command_line.command].run(command_line, settings)
    settings.save()
else:
    parser.print_help()
    sys.exit(1)
