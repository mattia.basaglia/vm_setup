import os
from shutil import copyfile
from utils.base_command import BaseCommand


class Setup(BaseCommand):
    """
    Configures a new VM
    """
    def arg_arguments(self, parser, settings):
        parser.add_argument(
            "--cpus", "-c",
            help="Number of CPUs",
            type=int,
            default=settings.get("vm_cpus", 1),
            dest="vm_cpus",
        )
        parser.add_argument(
            "--box", "-b",
            help="Vagrant source box",
            default=settings.get("vm_box", ""),
            dest="vm_box",
        )
        parser.add_argument(
            "--memory", "-m",
            help="RAM in Megs",
            default=settings.get("vm_memory", 1024),
            dest="vm_memory",
        )
        parser.add_argument(
            "--number", "-n",
            help="VM ID",
            type=int,
            default=settings.data.get("vm_id", 0) + 1,
        )
        parser.add_argument(
            "--ports", "-p",
            help="Ports to be forwarded",
            default=settings.get("net_ports", []),
            type=int,
            nargs="*",
        )
        parser.add_argument(
            "--synced-dest",
            help="Where to store synced dirs",
            default=settings.get("net_dirs_dest", "/var"),
        )
        parser.add_argument(
            "--synced-dirs", "-d",
            help="Directories to be synced",
            default=settings.get("net_dirs", []),
            nargs="*",
        )
        parser.add_argument(
            "--synced-dirs-readonly", "-dro",
            help="Directories to be synced (read-only)",
            default=settings.get("net_dirs_ro", []),
            nargs="*",
        )
        parser.add_argument(
            "--provision-script", "-s",
            help="Script for VM provisioning",
            default=settings.get("vm_provision", None),
        )
        parser.add_argument(
            "name",
            help="Hostname for this VM",
        )
        parser.add_argument(
            "--force", "-f",
            help="Overwrite existing configuration",
            default=False,
            action="store_true",
        )

    def run(self, command_line, settings):
        vm_number = "%02d" % command_line.number
        hostname = "%s-%s" % (vm_number, command_line.name)
        vagrantfile = hostname + ".rb"
        vagrantfile_path = settings.vm_path(vagrantfile)
        if not command_line.force and os.path.exists(vagrantfile_path):
                raise ValueError("VM already exists")

        if not os.path.exists(settings.vm_path()):
            os.makedirs(settings.vm_path())

        parent_vagrantfile = settings.vm_path("Vagrantfile")
        if command_line.force or not os.path.exists(parent_vagrantfile):
            copyfile(self.template_path("vagrantfile-parent.rb"), parent_vagrantfile)

        if command_line.number > settings.data["vm_id"]:
            settings.data["vm_id"] = command_line.number

        config_name = "config_%s_%s" % (vm_number, command_line.name)

        with open(vagrantfile_path, "w") as vagrant_file:
            ports = "\n".join(
                """{config_name}.vm.network "forwarded_port", guest: {port}, host: {port}{number}""".format(
                    config_name=config_name,
                    port=port,
                    number=vm_number,
                )
                for port in command_line.ports
            )

            provision = ""
            if command_line.provision_script:
                provision = '{config_name}.vm.provision "shell", path "{script}"'.format(
                    config_name=config_name,
                    script=command_line.provision_script
                )

            indent = " " * 8
            synced_dirs = "\n".join(
                indent + '{config_name}.vm.synced_folder "{path}", "{dest}", nfs: true, create: true'.format(
                    config_name=config_name,
                    path=path,
                    dest="%s/%s" % (command_line.synced_dest, os.path.basename(path))
                )
                for path in command_line.synced_dirs
            ) + "\n" + "\n".join(
                (indent + '{config_name}.vm.synced_folder "{path}", "{dest}", create: true, '
                 ':mount_options => ["ro"], owner: "root", group: "root"').format(
                    config_name=config_name,
                    path=path,
                    dest="%s/%s" % (command_line.synced_dest, os.path.basename(path))
                )
                for path in command_line.synced_dirs_readonly
            )

            template = open(self.template_path("vagrantfile-template.rb")).read()
            replacements = command_line.__dict__.copy()
            replacements.update(
                ports=ports,
                provision=provision,
                synced_dirs=synced_dirs,
                hostname=hostname,
                config_name=config_name,
            )

            vagrant_file.write(template.format(**replacements))

            print("Created %s" % hostname)

    def template_path(self, filename):
        return os.path.join(os.path.dirname(__file__), filename)
