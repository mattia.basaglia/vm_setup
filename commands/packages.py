from utils.base_command import BaseCommand


class InstallHostPackages(BaseCommand):
    """
    Installs packages on the host system (Ubuntu)
    """
    packages = [
        "vagrant", "libvirt-bin", "libvirt-dev", "nfs-server",
    ]

    def run(self, command_line, settings):
        self.cmd(["sudo", "apt-get"] + self.packages)
        self.cmd(["sudo", "-H", "vagrant", "plugin", "install", "vagrant-libvirt"])
