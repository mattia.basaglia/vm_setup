
Vagrant.configure(2) do |config|
    # The most common configuration options are documented and commented below.
    # For a complete reference, please see the online documentation at
    # https://docs.vagrantup.com.

    # Every Vagrant development environment requires a box. You can search for
    # boxes at https://atlas.hashicorp.com/search.
    config.vm.box = "{vm_box}"
    config.vm.provider "libvirt"

    config.vm.define "{hostname}" do |{config_name}|
        {config_name}.vm.hostname = "{hostname}"

        # Provider-specific configuration so you can fine-tune various
        # backing providers for Vagrant. These expose provider-specific options.
        # See https://github.com/vagrant-libvirt/vagrant-libvirt
        {config_name}.vm.provider :libvirt do |libvirt_settings|
            libvirt_settings.memory = {vm_memory}
            libvirt_settings.cpus = {vm_cpus}
        end

        {ports}
        {config_name}.vm.network :forwarded_port, guest: 22, host: 22{number}, id: 'ssh'
        {config_name}.vm.network :private_network, type: "dhcp"
        {config_name}.vm.network :private_network, ip: "192.168.121.{number}"
{synced_dirs}

        # Create a public network, which generally matched to bridged network.
        # Bridged networks make the machine appear as another physical device on
        # your network.
        # {config_name}.vm.network "public_network"

        {provision}
    end
end
