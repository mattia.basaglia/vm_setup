from utils.base_command import BaseCommand, ignore_command


@ignore_command
class VagrantBase(BaseCommand):
    """
    Runs a vagrant command
    """
    def arg_arguments(self, parser, settings):
        parser.add_argument(
            "--sudo", "-s",
            help="Run with sudo (default)",
            action="store_true",
            default=True,
        )
        parser.add_argument(
            "--no-sudo", "-S",
            help="Run without sudo",
            action="store_false",
            default=True,
            dest="sudo"
        )

    def vagrant_cmd(self, args):
        """
        Executes a vagrant command
        """
        # Libvirt needs sudo
        cmd = ["vagrant"] + args
        if self.sudo:
            cmd = ["sudo", "-H"] + cmd
        self.cmd(cmd, cwd=self.vm_path)

    def run(self, command_line, settings):
        self.sudo = command_line.sudo
        self.vm_path = settings.vm_path()

        self.run_vagrant(command_line, settings)

    def run_vagrant(self, command_line, settings):
        raise NotImplementedError()


class Vagrant(VagrantBase):
    """
    Runs a single vagrant command
    Note: you can also just invoke vegrant in the vm path
    """
    def arg_arguments(self, parser, settings):
        super(Vagrant, self).arg_arguments(parser, settings)
        parser.add_argument(
            "vagrant_command",
            nargs="+",
            help="vagrant commands",
        )

    def run_vagrant(self, command_line, settings):
        self.vagrant_cmd(command_line.vagrant_command)


class Shell(VagrantBase):
    """
    Attaches a shell to the virtual machine
    """
    def arg_arguments(self, parser, settings):
        super(Shell, self).arg_arguments(parser, settings)
        parser.add_argument(
            "machine",
            help="Machine name",
        )

    def run_vagrant(self, command_line, settings):
        self.vagrant_cmd(["ssh", command_line.machine])


class Up(VagrantBase):
    """
    Starts virtual machines
    """
    def arg_arguments(self, parser, settings):
        super(Up, self).arg_arguments(parser, settings)
        parser.add_argument(
            "machine",
            nargs="?",
            help="Machine name",
        )

    def run_vagrant(self, command_line, settings):
        cmd = ["up", "--provider=libvirt"]
        if command_line.machine:
            cmd += [command_line.machine]
        self.vagrant_cmd(cmd)


class Up(VagrantBase):
    """
    Stops virtual machines
    """
    def arg_arguments(self, parser, settings):
        super(Up, self).arg_arguments(parser, settings)
        parser.add_argument(
            "machine",
            nargs="?",
            help="Machine name",
        )

    def run_vagrant(self, command_line, settings):
        cmd = ["down"]
        if command_line.machine:
            cmd += [command_line.machine]
        self.vagrant_cmd(cmd)
