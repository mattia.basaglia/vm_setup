# -*- mode: ruby -*-
# vi: set ft=ruby :

vm_configs = Dir["*.rb"]
vm_configs.each do |vagrantfile|
    load File.expand_path(vagrantfile)
end
