import sys
from utils.base_command import BaseCommand


class GenerateSettings(BaseCommand):
    """
    Generates a settings files with the current settings
    (or the defaults if there are no current settings)
    """
    def arg_arguments(self, parser, settings):
        parser.add_argument(
            "--output", "-o",
            help="File to write to",
            default="-",
        )
        parser.add_argument(
            "--indent", "-i",
            help="Output indentation",
            default=4,
            type=int,
        )

    def run(self, command_line, settings):
        if command_line.output == "-":
            out = sys.stdout
        else:
            out = open(command_line.output, "w")
        settings.settings.to_json(out, indent=command_line.indent)
